---
layout: handbook-page-toc
title: Support Global Groups - FAQ
description: Support Global Groups FAQ
---

# Support Global Groups - FAQ
 
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Categories will be coming soon - meanwhile, here's everything

### Within SGG, how can we detect trends and do problem management?

- Take advantage of the facts that group views in SGG are globally visible and
  that broad collaboration is encouraged (Crush Sessions, Pairing, Senior Help
  Sessions, etc.). When working on a ticket, a Support Engineer can search
  Zendesk tickets, troubleshooting documents, product issues and Slack for
  similar situations that have already been reported. As FlexiPods become more
  widespread, they will provide an excellent space for trend detection as well.

### Within SGG, how can SEs find and take tickets related to their topic of study?

- When a Support Engineer is working on a training module, they should feel
  free to assign to themselves any tickets they find in any group's view that
  will help them in meeting their training requirements.

### How does SGG impact people’s sense of belonging to a single global team?

- It can have that undesired impact if we are not diligent at encouraging and
  enabling various opportunities for collaborating outside of a group. Two
  ideas for encouraging broad collaboration are:
  - Use cross-region crush sessions to collaborate with any peers 
  - Create **SGG Office Hours** to share tips and tricks between groups

### How are emergencies and escalations handled within SGG?

#### Emergencies

- We handle emergencies within SGG very much as we did previously. When an
  emergency ticket arrives the on-call engineer is paged. The ticket will not
  automatically get assigned to a group. The SE who takes assignment of the
  ticket should also assign the ticket to their group. Regardless of the group
  assignment, the SE should seek help from any and all people with the skills
  or knowledge needed to progress and solve the ticket.

#### Escalations

- We handle escalations within SGG very much as we did previously. The
  Manager-On-Call will in general reach out first to the assigned SE to
  communicate the details of the escalation and to determine whether the SE is
  available and able to give the ticket the needed attention. If they are not,
  the manager is encouraged to seek help from the SE's group both because
  they may already have knowledge of the ticket and because they are very
  likely to respond to the request. Finally, as before, the manager may reach
  out to the rest of Support if more help is needed.

### What is the manager’s role with respect to SGG?

- Managers are not aligned to specific groups
- Managers are available to work with any group that reaches out with
  questions, concerns, requests for guidance, requests for help, etc.
- Managers are encouraged to stay aware of group activity by watching the group
  Slack channels
- Managers work primarily with their own team members to understand challenges
  and growth opportunities, to provide coaching and guidance, etc.

### Is membership in a particular group permanent?

- Groups are intended to build camaraderie, comfort, a support system, and
  psychological safety. To do that, they should be as stable as possible over
  time. For that reason, mass rotations of group membership are not a part of
  the SGG plan. If groups want to transfer knowledge, they may consider a 1↔1
  member swap for a 1-2 week period. If a Support Engineer desires to make a
  long-term switch to another group, they should discuss their request with
  their manager, and the manager will facilitate the move if they agree with
  it.

### How do the L&R and USFed SEs fit within SGG?

- USFed SEs work as part of the USFed team, which operates in many ways like
  an SGG group.
- 100% L&R SEs work as part of the global L&R Team, which also operates
  similarly to an SGG group. SEs who have a partial L&R focus do work within
  an SGG group for their SaaS and SM responsibilities.
