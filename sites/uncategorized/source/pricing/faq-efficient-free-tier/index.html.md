---
layout: markdown_page
title: Efficient GitLab SaaS free tier
description: "On this page you can view frequently asked questions for changes related to free tier efficiency"
canonical_path: "/pricing/faq-efficient-free-tier/"
---

# Frequently Asked Questions - GitLab SaaS Free Tier

{:.no_toc}

### On this page

{:.no_toc}

{:toc}

- TOC

## User Limits on GitLab SaaS Free Tier

**Q. What is changing with user limits?**  
A. GitLab [announced](/blog/2022/03/24/efficient-free-tier)  updates to the user limits for the free tier of the GitLab SaaS offering. The new limits are as follows:  

| GitLab SaaS Tier                                                       |  Free |  
|----------------------------------------------------------|:-----:|  
| Price                                                                              |   $0  |  
| Users (per top level namespace) | 5 |  

**Q. Which users are these changes applicable to?**  
A. These user limits are applicable only to users of the free tier of the GitLab SaaS offering. These changes do not apply to the paid tiers, the free tier of the self-managed offering and [community programs](https://about.gitlab.com/community) - including GitLab for Open Source, Education and Startups users.

**Q. Do these changes apply to Trials?**  
A. No, the user limits do not apply to Trials during the trial period.

**Q. Are these changes applicable to public projects as well?**  
A. Yes, these changes are applicable to both public and private projects. Free tier users using GitLab for public Open Source projects should consider applying for the [GitLab for Open Source program](https://about.gitlab.com/solutions/open-source/) - which provides access to the GitLab Ultimate features and entitlements for free.

**Q. What is the effective date of the changes?**  
A. The new user limit on the free tier will be enforced starting on new and existing free SaaS namespaces starting June 22, 2022.

**Q. Will I be personally notified of the changes?**
A. Namespaces impacted by  this change will receive an in-product notification in the coming weeks. Those who do not engage with the in-app notification will also receive an email notification approximately 4 weeks prior to the effective date.

### Managing User Limits (Free tier only)

**Q. What does namespace in the context of user limits refer to?**  
A. In GitLab, a [namespace](https://docs.gitlab.com/ee/user/group/#namespaces) is a unique name for a user, a group, or subgroup under which a project can be created. User limits are implemented at the **top level group or personal namespace**.

**Q. How are the total number of users in my namespace calculated?**
A. We count the unique sum of users within a namespace which includes the users in the parent namespace (group), subgroups, and projects. For example:
If a user has a group named `top` and two sub-groups under `top` named `child1` and `child2` with 4 different unique users in each group, then the `top` namespace will have a total of 8 users, which is above the user limit of 5. If the two sub-groups contain the same 4 users, then `top` would only have 4 total users. 

**Q. Where can I view and manage the number of users in my namespace?**  
A. Users can view and manage the users in their namespace by going to their Group > Group Settings > Usage Quotas > Seats. Only Group Owners can add or remove users from the Usage Quotas page. Owners and Maintainers of individual sub-groups and projects can still manage the specific users in their sub-groups/projects.

**Q. How can I view and manage my users if my project is not located within a group?**  
A. If your project is not located within a group, you can manage the users in each of your personal projects but the sum of the unique members within all your personal projects cannot exceed 5.
GitLab strongly encourages personal projects to be moved into Groups which will allow these projects to access all GitLab features as well as give you the ability to manage all users from the Usage Quotas page, start a trial, and purchase a subscription.

**Q. What happens if I don’t reduce my user count before the enforcement date?**  
A. When the 5 user limit is applied, only the most recently active 5 users can continue to access the namespace. The remaining users will be moved to the over-limit user state. These users will not be able to access the namespace. The namespace owner will be able to manage users from the Usage Quotas page - including deleting users, moving users from over-limit to active and vice versa.

**Q. How can I add users beyond the limit of 5 users?**  
A. The free tier has a limit of 5 users which cannot be increased. GitLab recommends the paid tiers - Premium or Ultimate - for larger teams as there are no user limits and they contain features designed to increase your team’s productivity. We recommend starting a [free trial](https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=storage-usage-blog-post) of GitLab Ultimate to experience the value of the paid features while also getting access to unlimited users for the trial period.

Free tier users can also consider using the self-managed deployment option that does not have user limits. GitLab also has special programs for Open Source projects and students/educators granting access to GitLab Ultimate. If you believe you could qualify for one of these programs you can learn more [here](/community).

### More information

GitLab Premium and Ultimate have advanced capabilities and higher limits. Consider starting a free trial to experience GitLab Ultimate for 30 days or upgrading to a paid GitLab.com Tier by [purchasing online](https://customers.gitlab.com) or contacting [GitLab Sales](https://about.gitlab.com/sales) to unlock the full capabilities.

To address your questions and feedback, we have created a space in the [GitLab Community Forum](https://forum.gitlab.com/t/gitlab-introduces-user-limits-for-free-users-on-saas/64288), which is actively monitored by GitLab team members and product managers involved with this change.
